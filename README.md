# Git Training

This is a simple Git repository to be used as a baseline for a training program. 

## Basic Git commands

|Command|Description|Example|
|---|---|---|---|---|
|config --global --list|Lists the global configuration for your Git client|git config --global --list|
|config --global {property}|Lists the value of a configuration property|git config --global user.name|
|config --global color.ui|Enables coloring in Git client|git config --global color.ui auto|
|init|Initializes a git repo|git init|
|add|Adds a file to the repo staging area|git add README.md|
|commit|Commits a file to the Git repo|git commit -m "my required comment"|
|log|Lists version history|git log|
|diff|Shows file differences not yet staged|git diff|
|branch -a|List the branches in the local repo|git branch -a|
|branch {name}|Creates a new branch|git branch my_branch|
|checkout {name}|Switches to the branch|git checkout my_branch|
|merge {branch}|Merges the content of a branch into your current branch|git merge master|
|branch -d|Deletes a branch|git branch -d my_branch|
|checkout -b {name}|Shortcut to create branch and checkout|git checkout -b my_branch

## How to get this repository locally

git clone https://bitbucket.org/mgivney/git-training

## How to commit changes

* Create a branch
* Add and Commit changes to your branch
* Push your branch back to the repository with git push origin <your branch name>
